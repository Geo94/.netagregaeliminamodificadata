﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Cliente
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.CBFiltrar1 = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.Carga = New System.Windows.Forms.DataGridView()
        Me.Nuevo = New System.Windows.Forms.Button()
        Me.MCliente = New System.Windows.Forms.Button()
        Me.CEliminar = New System.Windows.Forms.Button()
        Me.CSalir = New System.Windows.Forms.Button()
        CType(Me.Carga, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(28, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Filtrar Por"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(51, 53)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "La Columna:"
        '
        'CBFiltrar1
        '
        Me.CBFiltrar1.FormattingEnabled = True
        Me.CBFiltrar1.Location = New System.Drawing.Point(133, 50)
        Me.CBFiltrar1.Name = "CBFiltrar1"
        Me.CBFiltrar1.Size = New System.Drawing.Size(270, 21)
        Me.CBFiltrar1.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(435, 53)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Filtrada Por:"
        '
        'ComboBox2
        '
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(513, 50)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(286, 21)
        Me.ComboBox2.TabIndex = 4
        '
        'Carga
        '
        Me.Carga.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Carga.Location = New System.Drawing.Point(31, 93)
        Me.Carga.Name = "Carga"
        Me.Carga.Size = New System.Drawing.Size(768, 408)
        Me.Carga.TabIndex = 5
        '
        'Nuevo
        '
        Me.Nuevo.Location = New System.Drawing.Point(218, 530)
        Me.Nuevo.Name = "Nuevo"
        Me.Nuevo.Size = New System.Drawing.Size(75, 43)
        Me.Nuevo.TabIndex = 6
        Me.Nuevo.Text = "Nuevo"
        Me.Nuevo.UseVisualStyleBackColor = True
        '
        'MCliente
        '
        Me.MCliente.Location = New System.Drawing.Point(312, 530)
        Me.MCliente.Name = "MCliente"
        Me.MCliente.Size = New System.Drawing.Size(77, 43)
        Me.MCliente.TabIndex = 7
        Me.MCliente.Text = "Modificar"
        Me.MCliente.UseVisualStyleBackColor = True
        '
        'CEliminar
        '
        Me.CEliminar.Location = New System.Drawing.Point(405, 530)
        Me.CEliminar.Name = "CEliminar"
        Me.CEliminar.Size = New System.Drawing.Size(73, 43)
        Me.CEliminar.TabIndex = 8
        Me.CEliminar.Text = "Eliminar"
        Me.CEliminar.UseVisualStyleBackColor = True
        '
        'CSalir
        '
        Me.CSalir.Location = New System.Drawing.Point(493, 530)
        Me.CSalir.Name = "CSalir"
        Me.CSalir.Size = New System.Drawing.Size(77, 43)
        Me.CSalir.TabIndex = 9
        Me.CSalir.Text = "Salir"
        Me.CSalir.UseVisualStyleBackColor = True
        '
        'Cliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(824, 585)
        Me.Controls.Add(Me.CSalir)
        Me.Controls.Add(Me.CEliminar)
        Me.Controls.Add(Me.MCliente)
        Me.Controls.Add(Me.Nuevo)
        Me.Controls.Add(Me.Carga)
        Me.Controls.Add(Me.ComboBox2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.CBFiltrar1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Cliente"
        Me.Text = "Todos Los Clientes"
        CType(Me.Carga, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents CBFiltrar1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents Carga As System.Windows.Forms.DataGridView
    Friend WithEvents Nuevo As System.Windows.Forms.Button
    Friend WithEvents MCliente As System.Windows.Forms.Button
    Friend WithEvents CEliminar As System.Windows.Forms.Button
    Friend WithEvents CSalir As System.Windows.Forms.Button

End Class
