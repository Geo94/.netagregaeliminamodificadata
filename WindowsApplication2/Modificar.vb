﻿Imports System.Data
Imports System.Data.SqlClient

Public Class Modificar
    Dim cn As New SqlConnection
    Dim conexion = "Data source=Localhost; Initial Catalog=Northwind; Persist Security Info=True; User ID=SA; Password=P@ssw0rd"


    Dim fila = Cliente.Carga.CurrentRow.Index

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            cn.ConnectionString = conexion
            'Call limpiarforma()


        Catch xcpsql As SqlException
            MessageBox.Show(" se ha producido un error:" & xcpsql.Message & " codigo error: " & xcpsql.ErrorCode)
        Catch ex As Exception
            MessageBox.Show("error de " & ex.Message)
        End Try

        CustomerID.Text = Cliente.Carga.Item(0, fila).Value
        CompanyName1.Text = Cliente.Carga.Item(1, fila).Value
        ContactTitle.Text = Cliente.Carga.Item(2, fila).Value
        ContactName.Text = Cliente.Carga.Item(3, fila).Value
        Country.Text = Cliente.Carga.Item(4, fila).Value
    End Sub

    Private Sub Modificar_Cliente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Modificar_Cliente.Click
        Try

            cn.Open()
            If CustomerID.Text <> "" Then
                Dim cmd As New SqlCommand("Modificar_client", cn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add("@CustomerID", SqlDbType.VarChar)
                cmd.Parameters.Add("@CompanyName", SqlDbType.VarChar)
                cmd.Parameters.Add("@ContactName", SqlDbType.VarChar)
                cmd.Parameters.Add("@ContactTitle", SqlDbType.VarChar)
                cmd.Parameters.Add("@Country", SqlDbType.VarChar)
                cmd.Parameters("@Customerid").Value = CustomerID.Text
                cmd.Parameters("@CompanyName").Value = CompanyName1.Text
                cmd.Parameters("@ContactName").Value = ContactName.Text
                cmd.Parameters("@ContactTitle").Value = ContactTitle.Text
                cmd.Parameters("@Country").Value = Country.Text

                cmd.ExecuteNonQuery()

            Else
                MessageBox.Show("debe de llenar el numero del el cliente")
            End If

        Catch xcpsql As SqlException
            If InStr(xcpsql.Message, "primary key") > 0 Then
                MessageBox.Show("el nombre de la compañia ya se encuentra registrado")
            Else
                MessageBox.Show("se ha producido el error:" & xcpsql.Message & "Codigo error:" & xcpsql.ErrorCode)
            End If
        Catch ex As Exception
            MessageBox.Show("error de " & ex.Message, "Nombre de compañia")
        Finally
            cn.Close()
        End Try
    End Sub

    Private Sub Cancelar_Click(sender As Object, e As EventArgs) Handles Cancelar.Click
        Call limpiarforma()
    End Sub

    Private Sub limpiarforma()
        Try
            For Each ocontrol1 As Control In Me.Controls
                If TypeOf ocontrol1 Is TextBox Then
                    ocontrol1.Text = ""
                End If
            Next
        Catch ex As Exception
            MessageBox.Show("error de" & ex.Message)
        End Try
    End Sub
End Class

