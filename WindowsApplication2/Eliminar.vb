﻿Imports System.Data
Imports System.Data.SqlClient

Public Class Eliminar
    Dim cn As New SqlConnection
    Dim conexion = "Data source=Localhost; Initial Catalog=Northwind; Persist Security Info=True; User ID=SA; Password=P@ssw0rd"
    Dim fila = Cliente.Carga.CurrentRow.Index


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            cn.ConnectionString = conexion
            'Call limpiarforma()


        Catch xcpsql As SqlException
            MessageBox.Show(" se ha producido un error:" & xcpsql.Message & " codigo error: " & xcpsql.ErrorCode)
        Catch ex As Exception
            MessageBox.Show("error de " & ex.Message)
        End Try

        CustomerID.Text = Cliente.Carga.Item(0, fila).Value
        CompanyName2.Text = Cliente.Carga.Item(1, fila).Value
        ContactTitle.Text = Cliente.Carga.Item(2, fila).Value
        ContactName.Text = Cliente.Carga.Item(3, fila).Value
        Country.Text = Cliente.Carga.Item(4, fila).Value
    End Sub

    Private Sub CEliminar_Click(sender As Object, e As EventArgs) Handles CEliminar.Click
        Try

            cn.Open()
            If CustomerID.Text <> "" Then
                Dim cmd As New SqlCommand("Eliminar_cliente", cn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add("@CustomerID", SqlDbType.VarChar)
                cmd.Parameters.Add("@CompanyName", SqlDbType.VarChar)
                cmd.Parameters.Add("@ContactName", SqlDbType.VarChar)
                cmd.Parameters.Add("@ContactTitle", SqlDbType.VarChar)
                cmd.Parameters.Add("@Country", SqlDbType.VarChar)
                cmd.Parameters("@Customerid").Value = CustomerID.Text
                cmd.Parameters("@CompanyName").Value = CompanyName2.Text
                cmd.Parameters("@ContactName").Value = ContactName.Text
                cmd.Parameters("@ContactTitle").Value = ContactTitle.Text
                cmd.Parameters("@Country").Value = Country.Text

                cmd.ExecuteNonQuery()
            End If


            If CustomerID.Text <> "" Then
                Dim cmd1 As New SqlCommand("Eliminar_detail", cn)
                cmd1.CommandType = CommandType.StoredProcedure
                cmd1.Parameters.Add("@CustomerID", SqlDbType.VarChar)
                cmd1.Parameters.Add("@CompanyName", SqlDbType.VarChar)
                cmd1.Parameters.Add("@ContactName", SqlDbType.VarChar)
                cmd1.Parameters.Add("@ContactTitle", SqlDbType.VarChar)
                cmd1.Parameters.Add("@Country", SqlDbType.VarChar)
                cmd1.Parameters("@CustomerID").Value = CustomerID.Text
                cmd1.Parameters("@CompanyName").Value = CompanyName2.Text
                cmd1.Parameters("@ContactName").Value = ContactName.Text
                cmd1.Parameters("@ContactTitle").Value = ContactTitle.Text
                cmd1.Parameters("@Country").Value = Country.Text

                cmd1.ExecuteNonQuery()

            End If

            If CustomerID.Text <> "" Then
                Dim cmd2 As New SqlCommand("Eliminar_ordenes", cn)
                cmd2.CommandType = CommandType.StoredProcedure
                cmd2.Parameters.Add("@CustomerID", SqlDbType.VarChar)
                cmd2.Parameters.Add("@CompanyName", SqlDbType.VarChar)
                cmd2.Parameters.Add("@ContactName", SqlDbType.VarChar)
                cmd2.Parameters.Add("@ContactTitle", SqlDbType.VarChar)
                cmd2.Parameters.Add("@Country", SqlDbType.VarChar)
                cmd2.Parameters("@CustomerID").Value = CustomerID.Text
                cmd2.Parameters("@CompanyName").Value = CompanyName2.Text
                cmd2.Parameters("@ContactName").Value = ContactName.Text
                cmd2.Parameters("@ContactTitle").Value = ContactTitle.Text
                cmd2.Parameters("@Country").Value = Country.Text

                cmd2.ExecuteNonQuery()
                MessageBox.Show("Esta seguro que desea Eliminar Al Cliente?")
            End If

        Catch xcpsql As SqlException
            If InStr(xcpsql.Message, "primary key") > 0 Then
                MessageBox.Show("el nombre de la compañia ya se encuentra registrado")
            End If
        Catch ex As Exception
            MessageBox.Show("Error  " & ex.Message, "Nombre de compañia")
        Finally
            cn.Close()
        End Try
    End Sub

    Private Sub Cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancelar.Click
        Call limpiarforma()
    End Sub

    Private Sub limpiarforma()
        Try
            For Each ocontrol1 As Control In Me.Controls
                If TypeOf ocontrol1 Is TextBox Then
                    ocontrol1.Text = ""
                End If
            Next
        Catch ex As Exception
            MessageBox.Show("error de" & ex.Message)
        End Try
    End Sub
End Class

