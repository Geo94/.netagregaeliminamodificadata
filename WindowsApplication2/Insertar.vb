﻿Imports System.Data
Imports System.Data.SqlClient


Public Class Insertar_Cliente
    Dim cn As New SqlConnection
    Dim total As Double = 0
    Dim totalcantidad As Integer = 0
    Dim nombre As String
    Dim producto As String
    Dim conexion As String = "Data source=Localhost; Initial Catalog=Northwind; Persist Security Info=True; User ID=SA; Password=P@ssw0rd"


    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            cn.connectionstring = conexion
            'Call limpiarforma()


        Catch xcpsql As SqlException
            MessageBox.Show(" se ha producido un error:" & xcpsql.Message & " codigo error: " & xcpsql.ErrorCode)
        Catch ex As Exception
            MessageBox.Show("error de " & ex.Message)
        End Try

    End Sub

    Private Sub Insertar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Insertar.Click
        Try

            cn.Open()
            If CustomerID.Text <> "" Then
                Dim cmd As New SqlCommand("insertar_cliente", cn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add("@Customerid", SqlDbType.VarChar)
                cmd.Parameters.Add("@Companyname", SqlDbType.VarChar)
                cmd.Parameters.Add("@ContactName", SqlDbType.VarChar)
                cmd.Parameters.Add("@Contacttitle", SqlDbType.VarChar)
                cmd.Parameters.Add("@Country", SqlDbType.VarChar)
                cmd.Parameters("@Customerid").Value = CustomerID.Text
                cmd.Parameters("@Companyname").Value = CompanyName1.Text
                cmd.Parameters("@Contactname").Value = ContactName.Text
                cmd.Parameters("@Contacttitle").Value = ContactTitle.Text
                cmd.Parameters("@Country").Value = Country.Text

                cmd.ExecuteNonQuery()

            Else
                MessageBox.Show("debe de llenar el numero del el cliente")
            End If

        Catch xcpsql As SqlException
            If InStr(xcpsql.Message, "primary key") > 0 Then
                MessageBox.Show("el nombre de la compañia ya se encuentra registrado")
            Else
                MessageBox.Show("se ha producido el error:" & xcpsql.Message & "Codigo error:" & xcpsql.ErrorCode)
            End If
        Catch ex As Exception
            MessageBox.Show("error de " & ex.Message, "Nombre de compañia")
        Finally
            cn.Close()
        End Try
    End Sub

    Private Sub Cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancelar.Click
        Call limpiarforma()
    End Sub
    Private Sub limpiarforma()
        Try
            For Each ocontrol1 As Control In Me.Controls
                If TypeOf ocontrol1 Is TextBox Then
                    ocontrol1.Text = ""
                End If
            Next
        Catch ex As Exception
            MessageBox.Show("error de" & ex.Message)
        End Try
    End Sub
End Class



