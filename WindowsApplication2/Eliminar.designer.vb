﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Eliminar
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.CustomerID = New System.Windows.Forms.TextBox()
        Me.CompanyName2 = New System.Windows.Forms.TextBox()
        Me.ContactName = New System.Windows.Forms.TextBox()
        Me.ContactTitle = New System.Windows.Forms.TextBox()
        Me.Country = New System.Windows.Forms.TextBox()
        Me.CEliminar = New System.Windows.Forms.Button()
        Me.Cancelar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(56, 45)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(62, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "CustomerID"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(56, 92)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(79, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "CompanyName"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(56, 140)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(72, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "ContactName"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(56, 184)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "ContactTitle"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(56, 231)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(43, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Country"
        '
        'CustomerID
        '
        Me.CustomerID.Location = New System.Drawing.Point(196, 42)
        Me.CustomerID.Name = "CustomerID"
        Me.CustomerID.Size = New System.Drawing.Size(100, 20)
        Me.CustomerID.TabIndex = 5
        '
        'CompanyName2
        '
        Me.CompanyName2.Location = New System.Drawing.Point(196, 89)
        Me.CompanyName2.Name = "CompanyName2"
        Me.CompanyName2.Size = New System.Drawing.Size(100, 20)
        Me.CompanyName2.TabIndex = 6
        '
        'ContactName
        '
        Me.ContactName.Location = New System.Drawing.Point(196, 137)
        Me.ContactName.Name = "ContactName"
        Me.ContactName.Size = New System.Drawing.Size(100, 20)
        Me.ContactName.TabIndex = 7
        '
        'ContactTitle
        '
        Me.ContactTitle.Location = New System.Drawing.Point(196, 181)
        Me.ContactTitle.Name = "ContactTitle"
        Me.ContactTitle.Size = New System.Drawing.Size(100, 20)
        Me.ContactTitle.TabIndex = 8
        '
        'Country
        '
        Me.Country.Location = New System.Drawing.Point(196, 228)
        Me.Country.Name = "Country"
        Me.Country.Size = New System.Drawing.Size(100, 20)
        Me.Country.TabIndex = 9
        '
        'CEliminar
        '
        Me.CEliminar.Location = New System.Drawing.Point(314, 87)
        Me.CEliminar.Name = "CEliminar"
        Me.CEliminar.Size = New System.Drawing.Size(75, 23)
        Me.CEliminar.TabIndex = 10
        Me.CEliminar.Text = "Eliminar"
        Me.CEliminar.UseVisualStyleBackColor = True
        '
        'Cancelar
        '
        Me.Cancelar.Location = New System.Drawing.Point(314, 135)
        Me.Cancelar.Name = "Cancelar"
        Me.Cancelar.Size = New System.Drawing.Size(75, 23)
        Me.Cancelar.TabIndex = 11
        Me.Cancelar.Text = "Cancelar"
        Me.Cancelar.UseVisualStyleBackColor = True
        '
        'Eliminar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(411, 287)
        Me.Controls.Add(Me.Cancelar)
        Me.Controls.Add(Me.CEliminar)
        Me.Controls.Add(Me.Country)
        Me.Controls.Add(Me.ContactTitle)
        Me.Controls.Add(Me.ContactName)
        Me.Controls.Add(Me.CompanyName2)
        Me.Controls.Add(Me.CustomerID)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Eliminar"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents CustomerID As System.Windows.Forms.TextBox
    Friend WithEvents CompanyName2 As System.Windows.Forms.TextBox
    Friend WithEvents ContactName As System.Windows.Forms.TextBox
    Friend WithEvents ContactTitle As System.Windows.Forms.TextBox
    Friend WithEvents Country As System.Windows.Forms.TextBox
    Friend WithEvents CEliminar As System.Windows.Forms.Button
    Friend WithEvents Cancelar As System.Windows.Forms.Button
End Class
