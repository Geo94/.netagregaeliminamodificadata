﻿Imports System.Data
Imports System.Data.SqlClient

Public Class Cliente
    Inherits System.Windows.Forms.Form
    Dim dvclientes As DataView
    Dim conexion As String = "Data source=Localhost; Initial Catalog=Northwind; Persist Security Info=True; User ID=SA; Password=P@ssw0rd"


    Private Sub carga1()
        Try
            Dim cn As New SqlConnection(conexion)
            Dim daclientes As New SqlDataAdapter("select customerid as [ID], CompanyName as [Company], Contactname as [Contact], contacttitle as [Title], Country as [Country] from customers", cn)
            Dim dsdatos As New DataSet
            daclientes.Fill(dsdatos, "customers")
            'no olvidemos que para filtrar necesitamos la vista"
            dvclientes = New DataView(dsdatos.Tables("customers"), "[ID] like '%'", "[Company] Asc ", DataViewRowState.OriginalRows)
            'origen del grid'
            Carga.DataSource = dvclientes
        Catch xcpsql As SqlException
            MessageBox.Show("Se ha producido un error:" & xcpsql.Message)
        Catch xcpinvop As System.InvalidOperationException
            MessageBox.Show("debe cerrar la conexion antes de volverla a abrir")
        Catch xpc As Exception
            MessageBox.Show("se ha producido el error:" & xpc.Message)
        End Try
    End Sub

    Private Sub form1_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        carga1()
    End Sub

    Private Sub Nuevo_Click(sender As Object, e As EventArgs) Handles Nuevo.Click
        Insertar_cliente.show()
    End Sub

    Private Sub Modificar_Click(sender As Object, e As EventArgs) Handles MCliente.Click
        Modificar.Show()
    End Sub

    Private Sub Eliminar_Click(sender As Object, e As EventArgs) Handles CEliminar.Click
        Eliminar.Show()
    End Sub

    Private Sub Salir_Click(sender As Object, e As EventArgs) Handles CSalir.Click
        Salir.Show()
    End Sub
End Class


