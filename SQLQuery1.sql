use Northwind
go

--Insertar cliente
create procedure insertar_cliente
@Customerid varchar(5), @Companyname varchar(100), @Contacttitle varchar(200), 
@Contactname varchar(100), @Country varchar(100) as
insert into customers (Customerid, Companyname, Contacttitle, Contactname,
Country) 
values (@Customerid, @Companyname, @Contacttitle, @Contactname, @Country)
go

--Modificar al cliente
create procedure Modificar_client
@CustomerID varchar(5), @Companyname varchar(100), @ContactTitle varchar(100),
@Contactname varchar(100), @Country varchar(100) as
update customers set  Companyname=@Companyname, 
Contacttitle=@Contacttitle, Contactname=@Contactname, Country=@Country
where CustomerID=@CustomerID
go

--Eliminar a los clientes
create procedure Eliminar_cliente
@CustomerID varchar(5), @CompanyName varchar(100), @ContactTitle varchar(100),
@ContactName varchar(100), @Country varchar(100) as
delete from Customers where CustomerID=@CustomerID
go

create procedure Eliminar_Detail
@Customerid varchar(5) 
as delete from [order details] where OrderID in (select CustomerID from Customers where CustomerID=@Customerid)
go

create procedure Eliminar_ordenes
@CustomerID varchar(5)
as delete from orders where CustomerID=@CustomerID